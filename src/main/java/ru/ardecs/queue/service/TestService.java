package ru.ardecs.queue.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import ru.ardecs.queue.model.Patient;
import ru.ardecs.queue.repository.PatientRepository;

@Service
@Transactional(propagation = Propagation.REQUIRES_NEW)
public class TestService {

    @Autowired
    private PatientRepository patientRepository;

    public Patient checkPatient(long phone) {
        return patientRepository.findByPhone(phone);
    }

    public Patient savePatient(Patient patient) {
        return patientRepository.save(patient);
    }
}

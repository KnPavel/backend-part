package ru.ardecs.queue.service;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.ardecs.queue.model.Booking;
import ru.ardecs.queue.model.Doctor;
import ru.ardecs.queue.model.Patient;
import ru.ardecs.queue.model.dto.TransferBookingData;
import ru.ardecs.queue.repository.BookingRepository;

@Service
public class PatientService implements ApplicationContextAware {

    @Autowired
    private BookingRepository bookingRepository;

    @Autowired
    private TestService testService;

    private ApplicationContext applicationContext;

    @Transactional
    public Booking storeBookingByPatientData(TransferBookingData data) {
        Patient patient = testService.checkPatient(data.getPhone());
        if (patient == null) {
            Patient newPatient = new Patient(data.getPhone(), data.getFirstName(), data.getLastName(),
                    data.getBirthday(), data.getProblem());
            patient = testService.savePatient(newPatient);
        }
        int interval = data.getTimeIntervalId();
        Doctor doctor = new Doctor(data.getDoctorId());
        Booking booking = new Booking(data.getDate(), doctor, patient, interval);
        PatientService bean = applicationContext.getBean(PatientService.class);
        System.out.println(bean.getClass().getName());
        return bookingRepository.save(booking);
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }
}

package ru.ardecs.queue.model;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Created by knpavel on 14.07.17.
 */
@Entity
@Table(name = "polyclinic")
public class Polyclinic extends BaseEntity {

    private String name;
    private String phone;

    protected Polyclinic() {
    }

    public Polyclinic(String name, String phone) {
        this.name = name;
        this.phone = phone;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}

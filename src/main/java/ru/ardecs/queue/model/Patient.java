package ru.ardecs.queue.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * Created by knpavel on 24.07.17.
 */
@Entity
public class Patient extends BaseEntity {

    @Column(unique = true)
    private long phone;
    @NotNull
    private String firstName;
    @NotNull
    private String lastName;
    @NotNull
    private String birthday;
    @NotNull
    private String problem;

    protected Patient() {

    }

    public Patient(long phone, String firstName, String lastName, String birthday, String problem) {
        this.phone = phone;
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthday = birthday;
        this.problem = problem;
    }

    public long getPhone() {
        return phone;
    }

    public void setPhone(long phone) {
        this.phone = phone;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getProblem() {
        return problem;
    }

    public void setProblem(String problem) {
        this.problem = problem;
    }
}

package ru.ardecs.queue.model.dto;

public class BookingStatistics {
    private long count;
    private int month;
    private String speciality;
    private int id;

    public BookingStatistics(long count, int month, String speciality, int id) {
        this.count = count;
        this.month = month;
        this.speciality = speciality;
        this.id = id;
    }

    public BookingStatistics(int count, int month, String speciality, int id) {
        this.count = count;
        this.month = month;
        this.speciality = speciality;
        this.id = id;
    }

    public long getCount() {
        return count;
    }

    public void setCount(long count) {
        this.count = count;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public String getSpeciality() {
        return speciality;
    }

    public void setSpeciality(String speciality) {
        this.speciality = speciality;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}

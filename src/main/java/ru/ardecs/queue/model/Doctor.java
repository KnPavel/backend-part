package ru.ardecs.queue.model;

import javax.persistence.*;
import java.util.List;

/**
 * Created by knpavel on 18.07.17.
 */
@Entity
@Table(name = "doctor")
public class Doctor extends BaseEntity {

    private String firstName;
    private String lastName;
    private int experience;

    @ManyToOne
    @JoinColumn(name = "speciality_id")
    private MedicalSpeciality medicalSpeciality;

    @ManyToOne
    @JoinColumn(name = "polyclinic_id")
    private Polyclinic polyclinic;

    @ManyToMany
    @JoinTable(name = "timetable", joinColumns = @JoinColumn(name = "doctor_id"),
            inverseJoinColumns = @JoinColumn(name = "time_interval_id"))
    private List<Interval> intervals;

    public Doctor() {
    }

    public Doctor(int id) {
        this.setId(id);
    }

    public Doctor(String firstName, String lastName, int experience, MedicalSpeciality medicalSpeciality,
                  Polyclinic polyclinic, List<Interval> intervals) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.experience = experience;
        this.medicalSpeciality = medicalSpeciality;
        this.polyclinic = polyclinic;
        this.intervals = intervals;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getExperience() {
        return experience;
    }

    public void setExperience(int experience) {
        this.experience = experience;
    }

    public MedicalSpeciality getMedicalSpeciality() {
        return medicalSpeciality;
    }

    public void setMedicalSpeciality(MedicalSpeciality medicalSpeciality) {
        this.medicalSpeciality = medicalSpeciality;
    }

    public Polyclinic getPolyclinic() {
        return polyclinic;
    }

    public void setPolyclinic(Polyclinic polyclinic) {
        this.polyclinic = polyclinic;
    }

    public List<Interval> getIntervals() {
        return intervals;
    }

    public void setIntervals(List<Interval> intervals) {
        this.intervals = intervals;
    }
}

package ru.ardecs.queue.model;

import javax.persistence.*;
import java.util.List;

/**
 * Created by knpavel on 24.07.17.
 */
@Entity
@Table(name = "time_interval")
public class Interval extends BaseEntity {

    @ManyToMany(mappedBy = "intervals")
    @Transient
    private List<Doctor> doctors;
    private String timeInterval;

    public Interval() {
    }

    public Interval(List<Doctor> doctors, String timeInterval) {
        this.doctors = doctors;
        this.timeInterval = timeInterval;
    }

    public List<Doctor> getDoctors() {
        return doctors;
    }

    public void setDoctors(List<Doctor> doctors) {
        this.doctors = doctors;
    }

    public String getTimeInterval() {
        return timeInterval;
    }

    public void setTimeInterval(String timeInterval) {
        this.timeInterval = timeInterval;
    }
}

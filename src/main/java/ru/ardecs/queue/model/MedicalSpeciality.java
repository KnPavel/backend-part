package ru.ardecs.queue.model;

import javax.persistence.*;

/**
 * Created by knpavel on 14.07.17.
 */
@Entity
@Table(name = "medical_speciality")
public class MedicalSpeciality extends BaseEntity {

    private String name;

    public MedicalSpeciality() {
    }

    public MedicalSpeciality(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

package ru.ardecs.queue.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.ardecs.queue.model.Polyclinic;
import ru.ardecs.queue.repository.DoctorRepository;

import java.util.List;

/**
 * Created by knpavel on 17.07.17.
 */
// TODO
// TODO - REST API - именование реализуемых REST методов должно соответствовать стандартам:
// TODO        1. http://microformats.org/wiki/rest/urls
// TODO        2. http://stackoverflow.com/questions/778203/are-there-any-naming-convention-guidelines-for-rest-apis)
@RestController
@RequestMapping("/polyclinic")
public class PolyclinicRestController {
    @Autowired
    private DoctorRepository doctorRepository;

    @GetMapping("/{speciality-id}")
    public List<Polyclinic> getPolyclinicBySpeciality(@PathVariable("speciality-id") Integer specialityId) {
        return doctorRepository.findPolyclinicBySpecialityId(specialityId);
    }
}
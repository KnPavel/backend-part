package ru.ardecs.queue.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.ardecs.queue.model.dto.BookingStatistics;
import ru.ardecs.queue.repository.BookingRepository;

import java.util.List;

@RestController
@RequestMapping("/statistic")
public class StatisticRestController {

    @Autowired
    private BookingRepository bookingRepository;

    @GetMapping
    public List<Integer> getYears() {
        return bookingRepository.getStatisticYear();
    }

    @GetMapping("/{year}")
    public List<BookingStatistics> getBookingByDoctorIdAndDate(@PathVariable("year") Integer year) {
        return bookingRepository.getBookingStatsByYear(year);
    }
}
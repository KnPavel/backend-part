package ru.ardecs.queue.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.ardecs.queue.model.MedicalSpeciality;
import ru.ardecs.queue.repository.SpecialityRepository;

import java.util.List;

/**
 * Created by knpavel on 18.07.17.
 */
@RestController
@RequestMapping("/speciality")
public class SpecialityRestController {
    @Autowired
    private SpecialityRepository specialityRepository;

    @GetMapping
    public Iterable<MedicalSpeciality> getSpecialityList() {
        return specialityRepository.findAll();
    }
}


package ru.ardecs.queue.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import ru.ardecs.queue.model.Booking;
import ru.ardecs.queue.model.dto.TransferBookingData;
import ru.ardecs.queue.service.PatientService;

import javax.validation.Valid;
import java.text.ParseException;

/**
 * Created by knpavel on 26.07.17.
 */
@RestController
@RequestMapping("/booking/patient/add")
public class PatientRestController {

    @Autowired
    private PatientService patientService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Booking patientData(@RequestBody @Valid TransferBookingData bookingData) throws ParseException {
        return patientService.storeBookingByPatientData(bookingData);
    }
}

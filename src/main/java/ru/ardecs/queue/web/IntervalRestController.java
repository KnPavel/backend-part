package ru.ardecs.queue.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ru.ardecs.queue.model.Interval;
import ru.ardecs.queue.repository.IntervalRepositories;

@RestController
@RequestMapping("/interval")
public class IntervalRestController {
	
	@Autowired
	private IntervalRepositories intervalRepositories;
	
	@GetMapping
	public Iterable<Interval> getInterval() {
		return intervalRepositories.findAll();
	}

}

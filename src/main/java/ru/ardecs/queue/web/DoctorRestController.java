package ru.ardecs.queue.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.ardecs.queue.model.Doctor;
import ru.ardecs.queue.repository.DoctorRepository;

import java.util.List;

/**
 * Created by knpavel on 19.07.17.
 */
@RestController
@RequestMapping("/doctor")
public class DoctorRestController {
    @Autowired
    public DoctorRepository doctorRepository;

    @GetMapping("/{polyclinic-id}&{speciality-id}")
    public List<Doctor> getDoctorsBySpecialityIdAndPolyclinicId(@PathVariable("speciality-id") Integer specialityId,
                                                                @PathVariable("polyclinic-id") Integer polyclinicId) {
        return doctorRepository.findDoctorByMedicalSpecialityIdAndPolyclinicId(polyclinicId, specialityId);
    }

    @GetMapping("/one/{id}")
    public Doctor getDoctorById(@PathVariable("id") Integer id) {
        return doctorRepository.findOne(id);
    }
}

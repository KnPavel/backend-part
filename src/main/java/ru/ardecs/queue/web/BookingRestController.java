package ru.ardecs.queue.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.ardecs.queue.model.Booking;
import ru.ardecs.queue.model.dto.TransferBookingData;
import ru.ardecs.queue.repository.BookingRepository;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

/**
 * Created by knpavel on 24.07.17.
 */
@RestController
@RequestMapping("booking")
public class BookingRestController {
    @Autowired
    private BookingRepository bookingRepository;

    @PostMapping("/{doctor-id}")
    public List<Booking> getBookingByDateAndDoctorId(@RequestBody Date date,
                                                     @PathVariable("doctor-id") Integer doctorId) throws ParseException {

        return bookingRepository.findAllByDateAndDoctorId(date, doctorId);
    }

    @GetMapping("/one/{doctor-id}")
    public Booking getBookingByDoctorId(@PathVariable("doctor-id") Integer doctorId) {
        return bookingRepository.findOne(doctorId);
    }
}

package ru.ardecs.queue.repository;

import org.springframework.data.repository.CrudRepository;
import ru.ardecs.queue.model.Polyclinic;

/**
 * Created by knpavel on 14.07.17.
 */
public interface PolyclinicRepository extends CrudRepository<Polyclinic, Integer> {
}

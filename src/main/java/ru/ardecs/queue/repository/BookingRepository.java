package ru.ardecs.queue.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import ru.ardecs.queue.model.Booking;
import ru.ardecs.queue.model.dto.BookingStatistics;

import java.util.Date;
import java.util.List;

/**
 * Created by knpavel on 24.07.17.
 */
public interface BookingRepository extends CrudRepository<Booking, Integer> {
    List<Booking> findAllByDateAndDoctorId(Date date, Integer doctorId);

    @Query("select new ru.ardecs.queue.model.dto.BookingStatistics(count(b.id), "
        + "function('MONTH', b.date), m.name, m.id) from Booking b join b.doctor d join d.medicalSpeciality m "
        + "where function('YEAR', b.date) = ?1 "
        + "group by function('MONTH', b.date), m.name, m.id")
    List<BookingStatistics> getBookingStatsByYear(Integer year);

    @Query("select distinct function('YEAR', b.date) from Booking b order by function('YEAR', b.date)")
    List<Integer> getStatisticYear();
}
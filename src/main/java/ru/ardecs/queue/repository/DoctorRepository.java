package ru.ardecs.queue.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import ru.ardecs.queue.model.Doctor;
import ru.ardecs.queue.model.Polyclinic;

import java.util.List;

/**
 * Created by knpavel on 18.07.17.
 */
public interface DoctorRepository extends CrudRepository<Doctor, Integer> {
    @Query("select distinct d.polyclinic from Doctor as d where d.medicalSpeciality.id = ?")
    List<Polyclinic> findPolyclinicBySpecialityId(Integer id);

    List<Doctor> findDoctorByMedicalSpecialityIdAndPolyclinicId(Integer specialityId, Integer polyclinicId);
}
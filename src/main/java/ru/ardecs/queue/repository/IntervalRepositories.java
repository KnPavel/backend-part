package ru.ardecs.queue.repository;

import org.springframework.data.repository.CrudRepository;
import ru.ardecs.queue.model.Interval;

/**
 * Created by knpavel on 24.07.17.
 */
public interface IntervalRepositories extends CrudRepository<Interval, Integer> {
}

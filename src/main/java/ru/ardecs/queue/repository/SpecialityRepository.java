package ru.ardecs.queue.repository;

import org.springframework.data.repository.CrudRepository;
import ru.ardecs.queue.model.MedicalSpeciality;

/**
 * Created by knpavel on 14.07.17.
 */
public interface SpecialityRepository extends CrudRepository<MedicalSpeciality, Integer> {
}
package ru.ardecs.queue.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import ru.ardecs.queue.model.Patient;

/**
 * Created by knpavel on 26.07.17.
 */
@Transactional(propagation = Propagation.REQUIRES_NEW)
public interface PatientRepository extends CrudRepository<Patient, Integer> {
    Patient findByPhone(long phone);
}
